package forecastBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
 
// run as:  C:\dev\Deployments>java -cp headlessForecastBuilder.jar forecastBuilder.Builder "test"
public class Builder {
	public static String keyword;
	static List<String> headerKeys; 
	static String currentHeader;
	String inputFileName;
	
	static { 
		headerKeys = new <String> ArrayList();
		
		headerKeys.add("FPUS5");
		headerKeys.add("FPUS7");
		headerKeys.add("WWUS8");
		headerKeys.add("NPOUS4");
		headerKeys.add("WWUS9");
		headerKeys.add("WUUS5");
		headerKeys.add("WWUS5");
		headerKeys.add("WWUS4");
		headerKeys.add("WFUS5");
		headerKeys.add("FXUS6");
		headerKeys.add("SXUS5");
		headerKeys.add("WWUS6");
		headerKeys.add("WGUS7");
		headerKeys.add("WGUS6");
		headerKeys.add("WGUS5");
		headerKeys.add("WGUS8");
		headerKeys.add("WWUS7");
		headerKeys.add("SRUS5");
	
	}

	public static void main(String args[]){
		
		if ( args.length == 0 || args.length > 1){
			System.out.println("Usage: forecastBuilder.Builder 'location'" );
			System.exit(0);
		}else {
			keyword = args[0];
			System.out.println("search phrase: " + keyword);
			Builder builder = new Builder();
			builder.build(args[0]);
		}
	}

	private void build(String keyword){
		ArrayList<String> rawList = doRead(keyword);
		String scrubbedData = parseRaw(rawList);
		buildResult(scrubbedData);
	}

	
	private File getInputFile(){
		
		File folder = new File("./input/");
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        inputFileName = file.getName();
		    	System.out.println("the input file: " + inputFileName);
		    }
		}
		return listOfFiles[0];
	}
	
	/*
	 * read data file
	 * if the current line has one of the header Keys, build the header
	 */

	private ArrayList<String> doRead(String keyword){
		ArrayList<String> dataBlockList = new ArrayList<String>();
		
	 	File inputFile = getInputFile();
		try(BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    boolean buildingHeader = false;
		    
		    while (line != null) {
		        line = br.readLine();
		        sb.append(line);
			    sb.append("\n");
	        System.out.println("reading in line: " + line);
		
		        for (String key : headerKeys){
			        if (line.contains(key)){
			        	buildingHeader = true;
			        	break;
			        }
			    }
		  
		       //build header blocks from $$ to ...
		        if ( buildingHeader){
		        	if (line.contains("...")){           
		        		for (String key : headerKeys){
				        	currentHeader = "";
				        
				        	//jk 1.22.2017 commenting to not build header
				        	//	currentHeader = sb.toString();
				        	buildingHeader = false;
				    //    	validateCurrentHeader();
				        	System.out.println("Built a header: " + currentHeader);
				        	break;
				        }
		        	}
		        }
		        
		        //build blocks from $$ to $$
		        if (line.contains("$$")){
		        	String currentBlock = sb.toString();

		        	// cut out all header stuff 1.22.17
		        //	String headerCheckedBlock = doHeaderCheck(currentBlock);
		        //	headerCheckedBlock = removeXML(headerCheckedBlock);
		        //	dataBlockList.add(headerCheckedBlock);
		       
		        	dataBlockList.add(currentBlock);
		        	sb = new StringBuilder();
		        }
		    }
		    
		}catch(Exception e){
			System.out.println("Exception reading.. " + e.getMessage());
		} 
		return dataBlockList;
	}

	
	private String removeXML(String s){
		//int iStart = s.indexOf("<?xml version=\"1.0\" standalone=\"yes\"?>");
		int iEnd = 0;
		if (s.contains("</site>")){
			iEnd = s.lastIndexOf("</site>");
		}
		
		
		String parsedXML = s.substring(iEnd + 7);
		return parsedXML;
	}
	
	private void validateCurrentHeader(){
		StringBuilder buff = new StringBuilder();	
		if( !currentHeader.contains(keyword)){
			String[] rows = currentHeader.split("\n");
			
			for(int i = 4; i < 8; i++){
				buff.append(rows[i]);
				buff.append('\n');
				
			}
			currentHeader = buff.toString();
		}
	}
		
	//if the current data from $$ to $$ does not contain a 'header' prepend it.  	
	private String doHeaderCheck(String dataBlock){
		boolean keyExists = false;
		for (String key : headerKeys){
			if (dataBlock.contains(key)){
				keyExists = true;
				break;
			}
		}
		
		if(!keyExists)
			dataBlock = currentHeader + "\n" + dataBlock;
		
		return dataBlock;
	}
	
	private String parseRaw(ArrayList<String> rawList){
		StringBuilder scrubbedData = new StringBuilder("<Headless Forcast Builder Test for " + keyword + " >");
		scrubbedData.append("\n");
		//go $$ to $$ - if 'input' found, stick in map otherwise carry on
		
		int keywordCount = 0;
		for (String s : rawList){
	//		System.out.println("Current String: " + s);
			if (s.toUpperCase().contains(keyword.toUpperCase())){
				System.out.println("group #" + keywordCount + " contains search phrase " + keyword);
				System.out.println("the text: " + s);
				scrubbedData.append(s);
				scrubbedData.append("\n");
				keywordCount++;
			}
		}
		
		scrubbedData.append(keywordCount + " instances of " + keyword + " found...");
		System.out.println(keywordCount + " instances of " + keyword + " found...");
		
		return scrubbedData.toString();
	}

 
	
	private void buildResult(String data){
		
		try {
			String fileName = inputFileName + "_" + keyword + ".txt";
			File file = new File("./output/" + fileName);
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(data);
	 		fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
