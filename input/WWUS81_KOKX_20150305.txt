
480 
WWUS81 KOKX 051000
SPSOKX

SPECIAL WEATHER STATEMENT
NATIONAL WEATHER SERVICE NEW YORK NY
500 AM EST THU MAR 5 2015

CTZ005>012-NJZ002-004-103>105-NYZ067>071-051130-
EASTERN BERGEN-EASTERN PASSAIC-NORTHERN FAIRFIELD-NORTHERN
MIDDLESEX-NORTHERN NEW HAVEN-NORTHERN NEW LONDON-NORTHERN
WESTCHESTER-ORANGE-PUTNAM-ROCKLAND-SOUTHERN FAIRFIELD-SOUTHERN
MIDDLESEX-SOUTHERN NEW HAVEN-SOUTHERN NEW LONDON-SOUTHERN
WESTCHESTER-WESTERN BERGEN-WESTERN ESSEX-WESTERN PASSAIC-
500 AM EST THU MAR 5 2015

...LIGHT TO MODERATE SNOW WILL IMPACT 
BERGEN...FAIRFIELD...MIDDLESEX...NEW HAVEN...NEW LONDON...NORTHERN 
ESSEX...ORANGE...PASSAIC...PUTNAM...ROCKLAND AND WESTCHESTER 
COUNTIES...

AT 452 AM EST...NATIONAL WEATHER SERVICE DOPPLER RADAR WAS TRACKING 
LIGHT TO MODERATE SNOW DEVELOPING OVER THE REGION. BETWEEN 1 TO 2 
INCHES OF SNOW IS POSSIBLE BY 8 AM.

WITH VISIBILITIES DROPPING TO 1/2 MILE AT TIMES...AND TEMPERATURES 
DROPPING BELOW FREEZING THROUGH 8 AM...DETERIORATING ROAD CONDITIONS 
CAN BE EXPECTED.

MOTORISTS SHOULD EXERCISE EXTREME CAUTION THIS MORNING.

$$

NV







573 
WWUS81 KOKX 051014
SPSOKX

SPECIAL WEATHER STATEMENT
NATIONAL WEATHER SERVICE NEW YORK NY
514 AM EST THU MAR 5 2015

NJZ006-105>108-NYZ072>075-078>081-176>179-051215-
BRONX-EASTERN ESSEX-EASTERN UNION-HUDSON-KINGS (BROOKLYN)-NEW YORK
(MANHATTAN)-NORTHEAST SUFFOLK-NORTHERN NASSAU-NORTHERN
QUEENS-NORTHWEST SUFFOLK-RICHMOND (STATEN IS.)-SOUTHEAST
SUFFOLK-SOUTHERN NASSAU-SOUTHERN QUEENS-SOUTHWEST SUFFOLK-WESTERN
ESSEX-WESTERN UNION-
514 AM EST THU MAR 5 2015

...SNOW WILL IMPACT MORNING COMMUTE FOR THE BRONX...HUDSON...KINGS 
(BROOKLYN)...NASSAU...NEW YORK (MANHATTAN)...QUEENS...RICHMOND 
(STATEN ISLAND)...SOUTHERN ESSEX...SUFFOLK AND UNION COUNTIES...

AT 507 AM EST...NATIONAL WEATHER SERVICE DOPPLER RADAR WAS TRACKING 
LIGHT TO MODERATE SNOW DEVELOPING OVER THE REGION. ONE TO TWO INCHES 
OF SNOW ARE POSSIBLE BY 8 AM.

WITH VISIBILITIES DROPPING TO 1/2 MILE AT TIMES AND TEMPERATURES 
DROPPING TO OR JUST BELOW FREEZING THROUGH 8 AM...DETERIORATING ROAD 
CONDITIONS CAN BE EXPECTED.

MOTORISTS SHOULD EXERCISE EXTREME CAUTION DURING THIS MORNING 
COMMUTE.

$$

NV







572 
WWUS81 KOKX 051103
SPSOKX

SPECIAL WEATHER STATEMENT
NATIONAL WEATHER SERVICE NEW YORK NY
603 AM EST THU MAR 5 2015

NJZ006-105>108-NYZ072>075-078>081-176>179-051245-
BRONX-EASTERN ESSEX-EASTERN UNION-HUDSON-KINGS (BROOKLYN)-NEW YORK
(MANHATTAN)-NORTHEAST SUFFOLK-NORTHERN NASSAU-NORTHERN
QUEENS-NORTHWEST SUFFOLK-RICHMOND (STATEN IS.)-SOUTHEAST
SUFFOLK-SOUTHERN NASSAU-SOUTHERN QUEENS-SOUTHWEST SUFFOLK-WESTERN
ESSEX-WESTERN UNION-
603 AM EST THU MAR 5 2015

...MODERATE TO LOCALLY HEAVY SNOW WILL IMPACT BRONX...HUDSON...KINGS 
(BROOKLYN)...NASSAU...NEW YORK (MANHATTAN)...QUEENS...RICHMOND 
(STATEN ISLAND)...SOUTHERN ESSEX...SUFFOLK AND UNION COUNTIES FOR 
THE MORNING COMMUTE

AT 600 AM EST...NATIONAL WEATHER SERVICE DOPPLER RADAR WAS TRACKING 
MODERATE TO LOCALLY HEAVY SNOW DEVELOPING OVER THE AREA.

SNOWFALL RATES OF ONE HALF TO ONE INCH PER HOUR AND VISIBILITIES OF 
1/2 TO 1/4 MILE OR LESS ARE EXPECTED TO DEVELOP THROUGH 8AM.
 
TEMPERATURES DROPPING BELOW FREEZING FROM MID-MORNING ON WILL RESULT 
IN ICING ON UNTREATED ROADS.

EXPECT TRAVEL TO BECOME INCREASINGLY DIFFICULT THROUGH THE MORNING 
RUSH.

A WINTER STORM WARNING REMAINS IN EFFECT FOR THE AREA.

$$

NV







691 
WWUS81 KOKX 051545
SPSOKX

SPECIAL WEATHER STATEMENT
NATIONAL WEATHER SERVICE NEW YORK NY
1045 AM EST THU MAR 5 2015

CTZ005-006-NYZ067-068-051900-
NORTHERN FAIRFIELD-NORTHERN NEW HAVEN-ORANGE-PUTNAM-
1045 AM EST THU MAR 5 2015

...SNOW WITH SLIPPERY TRAVEL THROUGH EARLY THIS AFTERNOON...

PERIODS OF LIGHT TO OCCASIONALLY MODERATE SNOW WILL FALL AS TEMPERATURES
FALL TO NEAR 20 DEGREES THROUGH EARLY AFTERNOON.

THE STORM TOTAL SNOWFALL FORECAST THROUGH THIS TIME IS 1 TO 3 INCHES.

MOTORISTS SHOULD EXERCISE CAUTION WHILE DRIVING ON SLIPPERY ROADS
DURING THIS TIME.

$$
GC




